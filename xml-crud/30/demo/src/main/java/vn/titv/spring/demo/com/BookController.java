package vn.titv.spring.demo.com;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/books")
public class BookController {

    private List<Book> bookList = new ArrayList<>();

    @Autowired
    private ObjectMapper xmlMapper;


    // **** add xml
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public Books addBook(@RequestBody Book newBook) throws IOException {
        // Thêm book mới vào danh sách
        bookList.add(newBook);

        // Tạo danh sách mới chứa tất cả book
        Books books = new Books();
        books.setBookList(bookList);

        // Chuyển đổi danh sách mới thành chuỗi xml
        String xml = xmlMapper.writeValueAsString(books);
        books = xmlMapper.readValue(xml, Books.class);

        return books;
    }


}

