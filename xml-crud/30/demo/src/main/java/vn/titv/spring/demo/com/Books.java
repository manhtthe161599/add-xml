package vn.titv.spring.demo.com;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "books")
public class Books {
    private List<Book> book;

    @JacksonXmlElementWrapper
    public List<Book> getBook() {
        return book;
    }

    public void setBookList(List<Book> book) {
        this.book = book;
    }
}

